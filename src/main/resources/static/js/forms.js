const token = $("meta[name='_csrf']").attr("content");
const header = $("meta[name='_csrf_header']").attr("content");
const ajaxContextPath = window.location.pathname.split("/")[1];
const apiContextPath = "/" + ajaxContextPath;
let mainDatatable;

$(document).ajaxSend(function(e, xhr, options) {
    xhr.setRequestHeader(header, token);
});

const defSuccessMessage = 'Cambios guardados correctamente';
const defErrorMessage = 'Error en guardado de datos';

function ajaxRequestGet(url, data) {
    return $.ajax({
        url: url,
        type: 'GET',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000
    });
}

function ajaxRequestMethod(type, url, dataObject) {
    return $.ajax({
        url: url,
        type: type,
        data: JSON.stringify(dataObject),
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000
    });
}

jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-valid');
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
    }
});

function initMainDatatable(type, url, columns, columnDefs, funcRow, dataPath) {
    mainDatatable = $('#mainTable').DataTable({
        "language": {"url": apiContextPath + "/js/Spanish.json"},
        "lengthMenu": [[5, 10, 20, -1], [5, 10, 20, "Todos"]],
        "bProcessing": true,
        "bServerSide": true,
        "pageLength": 5,
        "bSearching": false,
        "bSearchable": false,
        "bLengthChange": false,
        "bSort": false,
        "paging": true,
        "dom": '<"top">rt<"bottom"p><"clear">',
        ajax: function (data, refreshDataTableCallback, settings) {
            $.ajax({
                url: apiContextPath + url,
                type: type,
                contentType: 'application/json; charset=utf-8',
                data: buildTableRequestData(data),
                datatype: 'json',
                success: function (response) {
                    refreshDataTableCallback({
                            data: response['_embedded'][dataPath],
                            recordsTotal: response['page']['totalElements'],
                            pageNumber: response['page']['number'],
                            totalPages: response['page']['totalPages'],
                            recordsFiltered: response['page']['totalPages'] * data.length,
                            message: null,
                    });
                }
            });
        },
        "columns": columns,
        "aaSorting": [],
        "fnRowCallback": validateDatatableRowFunction(funcRow)
    });
}

function reloadMainDatatable() {
    if (mainDatatable !== undefined && mainDatatable !== null) {
        mainDatatable.ajax.reload();
    }
}

function clearValidation(formElement){
    document.querySelectorAll('.CRUD-element').forEach(item => {
        item.value = null;
        $(item).removeClass('is-valid');
        $(item).removeClass('is-invalid');
    });
}

$("form").on("input change", ".CRUD-element", function (event) {
    let target = $( event.target );
    let name = $(this).attr("name");
    let type = $(this).attr("type");
    if (target.is('input') && !$(this).is('[readonly]')) {
        if (type === 'text') {
            CRUDModel[name] = $(this).val();
        } else if(type === 'checkbox') {
            CRUDModel[name] = $(this).is(":checked");
        } else if (type === 'hidden') {
            CRUDModel[name] = $(this).val();
        } else if (type === 'file') {
            let file = $(this)[0].files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                CRUDModel[name] = reader.result;
            }
        } else {
            CRUDModel[name] = $(this).val();
        }
    } else if (target.is('select')) {
        CRUDModel[name] = [];
        CRUDModel[name] = $(this).val();
        let selectData = $(this).find(":selected").data("value");
        if (selectData !== undefined && selectData !== null) {
            CRUDModel[name] = selectData;
        }
    } else if(target.is('textarea')) {
        CRUDModel[name] = $(this).val();
    } else {
        console.error("CRUD element not supported")
    }
});

function validateDatatableRowFunction(f) {
    if (f !== null && f !== undefined) {
        return f;
    } else {
        return function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if (aData['visible'] === false){
                $('td', nRow).addClass("table-danger");
                $('td:last-child', nRow).empty();
                $('td:last-child', nRow).append('<span class="badge badge-pill badge-danger">No disponible</span>')
            }
        }
    }
}

$('.required-input').tooltip({ boundary: 'window', title: 'Campo obligatorio' })

/* UTILS */
function formatDate(date) {
    if (date !== undefined && date !== null && date.length > 0) {
        let dateObj = new Date(date);
        return dateObj.getDate() + ' de ' + monthNames[dateObj.getMonth()] + ' ' + dateObj.getFullYear();
    } else {
        return date;
    }

}

function formatBoolean(bool) {
    if (bool) {
        return '<span class="badge badge-pill badge-success"><i class="mdi mdi-checkbox-marked-circle-outline"></i></span>';
    } else {
        return '<span class="badge badge-pill badge-danger"><i class="mdi mdi-close-circle-outline"></i></span>';
    }
}

function noEmptyText(value, defaultValue = "...") {
    if (value === undefined || value === null || value.length === 0){
        return defaultValue;
    } else {
        return value;
    }
}

function buildTableRequestData(data) {
    data['size'] = data.length;
    data['page'] = data.start/data.length;
    return data;
}