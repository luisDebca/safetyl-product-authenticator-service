package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.dto.MailModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Slf4j
@Service
public class MailServiceImpl implements MailService{

    private static final String INLINE_CONTENT_TYPE = "image/png";
    private final Locale esLocale = new Locale("es", "ES");
    private final JavaMailSender mailSender;
    private final SpringTemplateEngine templateEngine;

    public MailServiceImpl(JavaMailSender mailSender, SpringTemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public void sendEmail(MailModel model) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            log.info("Prenparando envio de correo electronico.");
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
            log.info("Obteniendo informacion de empresa.");

            Context context = new Context(esLocale);
            context.setVariables(model.getModel());
            log.info("Obteniendo html template para envio.");
            String html = templateEngine.process(model.getTemplate(), context);
            helper.setTo(model.getTo());
            helper.setFrom("noreply@safetyl.com");
            helper.setSubject(model.getSubject());
            helper.setText(html, true);
            helper.setValidateAddresses(true);
            helper.addInline("company_logo_key.png", getLogoIcon(), INLINE_CONTENT_TYPE);
            helper.addInline("app_icon_key.png", getAppIcon(), INLINE_CONTENT_TYPE);
            log.info("Enviando correos electronicos...");
            mailSender.send(message);
            log.info("Envio de correos finalizado.");
        } catch (MessagingException e) {
            log.error("Error preparando envio de correo: ", e.getCause());
        } catch (Exception e) {
            log.error("Error en envio de correos: {}", e.getMessage());
            log.error("Error fatal", e.getCause());
        }
    }

    private Resource getLogoIcon() {
        return new ClassPathResource("/static/img/ues.png");
    }

    private Resource getAppIcon() {
        return new ClassPathResource("/static/img/safetyl.png");
    }


}
