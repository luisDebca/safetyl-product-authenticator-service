package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.model.UserApp;
import edu.ues.safetylproductauthenticatorservice.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void save(UserApp userApp) {
        userApp.setPassword(passwordEncoder.encode(userApp.getPassword()));
        repository.save(userApp);
    }

    @Override
    public void update(UserApp userApp, String username) {
        UserApp user = repository.findByUsername(username);
        if (user != null) {
            user.setEmail(userApp.getEmail());
            if (!user.getPassword().equals(userApp.getPassword())) {
                user.setPassword(passwordEncoder.encode(userApp.getPassword()));
            }
        }
    }
}
