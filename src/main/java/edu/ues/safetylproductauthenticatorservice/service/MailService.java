package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.dto.MailModel;

public interface MailService {

    void sendEmail(MailModel mailModel);
}
