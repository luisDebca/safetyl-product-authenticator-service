package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.dto.ProductValidationResponse;
import edu.ues.safetylproductauthenticatorservice.model.History;
import edu.ues.safetylproductauthenticatorservice.model.License;
import edu.ues.safetylproductauthenticatorservice.model.Product;
import edu.ues.safetylproductauthenticatorservice.model.VerificationResult;
import edu.ues.safetylproductauthenticatorservice.repository.HistoryRepository;
import edu.ues.safetylproductauthenticatorservice.repository.LicenseRepository;
import edu.ues.safetylproductauthenticatorservice.repository.OwnerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductValidationServiceImpl implements ProductValidationService {

    private final OwnerRepository ownerRepository;
    private final LicenseRepository licenseRepository;
    private final HistoryRepository historyRepository;

    public ProductValidationServiceImpl(OwnerRepository ownerRepository, LicenseRepository licenseRepository, HistoryRepository historyRepository) {
        this.ownerRepository = ownerRepository;
        this.licenseRepository = licenseRepository;
        this.historyRepository = historyRepository;
    }

    @Override
    @Transactional
    public ProductValidationResponse validate(String owner, String serial) {
        log.info("Validating product ...");
        log.info("Product owner: {}", owner);
        log.info("Product serial: {}", serial);
        VerificationResult verificationResult = VerificationResult.FAIL;
        List<License> licenses = licenseRepository.getByOwner(owner);
        boolean anyMatch = licenses.stream().anyMatch(x -> x.getSerial().equals(serial));
        log.info("Is valid product ?: {}", anyMatch);
        ProductValidationResponse response = new ProductValidationResponse();
        response.setValidationResult(anyMatch);
        response.setValidateAt(LocalDateTime.now());

        History history = new History();
        history.setVerificationDate(LocalDateTime.now());
        if (anyMatch) {
            Optional<License> license = licenses.stream().filter(x -> x.getSerial().equals(serial)).findFirst();
            if (license.isPresent()) {
                response.setProducts(licenseRepository.findAllProducts(license.get().getId()).stream().map(Product::getName).collect(Collectors.toList()));
                response.setExpiredAt(license.get().getExpireDate());
                history.setProductList(String.join(",", response.getProducts()));
                log.info("Creating history record.");
                history.setLicense(license.get());
                verificationResult = getValidationResult(license.get().getEnable() && isNotExpired(license.get().getActivationDate(), license.get().getExpireDate()));
                response.setVerificationResult(verificationResult);
                licenseRepository.updateLastVerification(license.get().getId(), LocalDateTime.now());
            }
        }
        history.setVerificationResult(verificationResult);
        history.setProductOwner(owner);
        history.setProductSerial(serial);
        historyRepository.save(history);

        return response;
    }

    private VerificationResult getValidationResult(boolean isValid) {
        return isValid ? VerificationResult.SUCCESS : VerificationResult.EXPIRED;
    }

    private boolean isNotExpired(LocalDateTime start, LocalDateTime end) {
        LocalDateTime today = LocalDateTime.now();
        return today.isAfter(start) && today.isBefore(end);
    }
}
