package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.model.License;

public interface LicenseService {

    void save(License license);

    void update(License license);

    void disable(Long id);

    void enable(Long id);
}
