package edu.ues.safetylproductauthenticatorservice.service;

import com.github.olili2017.O2019.SerialNumberGenerator;
import edu.ues.safetylproductauthenticatorservice.model.License;
import edu.ues.safetylproductauthenticatorservice.repository.LicenseRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
public class LicenseServiceImpl implements LicenseService {

    @Value("${serial.number.generator.length}")
    private int serialNumberGeneratorLength;

    @Value("${serial.number.generator.split}")
    private int serialNumberGeneratorSplit;

    @Value("${serial.number.generator.separator}")
    private char serialNumberGeneratorSeparator;

    private final LicenseRepository licenseRepository;

    public LicenseServiceImpl(LicenseRepository licenseRepository) {
        this.licenseRepository = licenseRepository;
    }

    @Override
    @Transactional
    public void save(License license) {
        log.info("Saving new license...");
        String serial = new SerialNumberGenerator(serialNumberGeneratorLength).split(serialNumberGeneratorSplit, serialNumberGeneratorSeparator).generate();
        license.setEnable(true);
        license.setSerial(serial);
        license.setMaxInstanceEnable(1);
        licenseRepository.save(license);
    }

    @Override
    @Transactional
    public void update(License license) {
        log.info("Updating license...");
        Optional<License> optional = licenseRepository.findById(license.getId());
        if (optional.isPresent()) {
            optional.get().setActivationDate(license.getActivationDate());
            optional.get().setExpireDate(license.getExpireDate());
            optional.get().setMaxUserEnabled(license.getMaxUserEnabled());
        }
    }

    @Override
    @Transactional
    public void disable(Long id) {
        log.info("Disabling license...");
        licenseRepository.enable(id, false);
    }

    @Override
    @Transactional
    public void enable(Long id) {
        log.info("Enabling license...");
        licenseRepository.enable(id, true);
    }
}
