package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.model.UserApp;

public interface UserService {

    void save(UserApp userApp);

    void update(UserApp userApp, String username);

}
