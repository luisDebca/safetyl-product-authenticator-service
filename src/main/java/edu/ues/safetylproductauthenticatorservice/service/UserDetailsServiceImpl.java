package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.model.UserApp;
import edu.ues.safetylproductauthenticatorservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserApp user = userRepository.findByUsername(username);
        if (user != null) {
            log.info("User validated !!!");
            return new User(user.getUsername(), user.getPassword(), Collections.emptyList());
        } else {
            log.error("User with name {} not found", username);
            throw new UsernameNotFoundException("User not found.");
        }
    }
}
