package edu.ues.safetylproductauthenticatorservice.service;

import edu.ues.safetylproductauthenticatorservice.dto.ProductValidationResponse;

public interface ProductValidationService {

    ProductValidationResponse validate(String owner, String serial);
}
