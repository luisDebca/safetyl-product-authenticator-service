package edu.ues.safetylproductauthenticatorservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/manager/history")
public class HistoryController {

    @GetMapping
    public String history(Principal principal, Model model) {
        model.addAttribute("username", principal.getName());
        return "history";
    }
}
