package edu.ues.safetylproductauthenticatorservice.controller;

import edu.ues.safetylproductauthenticatorservice.dto.ResponseDTO;
import edu.ues.safetylproductauthenticatorservice.model.License;
import edu.ues.safetylproductauthenticatorservice.service.LicenseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/licenses")
public class LicenseRestController {

    private final LicenseService licenseService;

    public LicenseRestController(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @PostMapping()
    public ResponseEntity<Object> save(@RequestBody License license) {
        try {
            licenseService.save(license);
            return new ResponseEntity<>(new ResponseDTO().ok(license), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error: {}", e.getMessage());
            return new ResponseEntity<>(new ResponseDTO().error(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping
    public ResponseEntity<Object> update(@RequestBody License license) {
        try {
            licenseService.update(license);
            return new ResponseEntity<>(new ResponseDTO().ok(license), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error: {}", e.getMessage());
            return new ResponseEntity<>(new ResponseDTO().error(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/enable")
    public ResponseEntity<Object> enable(@PathVariable Long id) {
        licenseService.enable(id);
        return new ResponseEntity<>(new ResponseDTO().ok(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/disable")
    public ResponseEntity<Object> disable(@PathVariable Long id) {
        licenseService.disable(id);
        return new ResponseEntity<>(new ResponseDTO().ok(id), HttpStatus.OK);
    }
}
