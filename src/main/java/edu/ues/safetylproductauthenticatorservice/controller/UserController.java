package edu.ues.safetylproductauthenticatorservice.controller;

import edu.ues.safetylproductauthenticatorservice.dto.ResponseDTO;
import edu.ues.safetylproductauthenticatorservice.model.UserApp;
import edu.ues.safetylproductauthenticatorservice.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> save(@RequestBody UserApp userApp) {
        userService.save(userApp);
        return new ResponseEntity<>(new ResponseDTO().ok("User save success"), HttpStatus.OK);
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<ResponseDTO> update(@RequestBody UserApp userApp, @PathVariable String username) {
        userService.update(userApp, username);
        return new ResponseEntity<>(new ResponseDTO().ok("User update success"), HttpStatus.OK);
    }

}
