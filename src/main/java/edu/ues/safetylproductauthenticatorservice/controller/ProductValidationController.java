package edu.ues.safetylproductauthenticatorservice.controller;

import edu.ues.safetylproductauthenticatorservice.dto.ProductValidationResponse;
import edu.ues.safetylproductauthenticatorservice.service.ProductValidationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/validate-product")
public class ProductValidationController {

    private final ProductValidationService validationService;

    public ProductValidationController(ProductValidationService validationService) {
        this.validationService = validationService;
    }

    @GetMapping
    public ResponseEntity<ProductValidationResponse> validate(@RequestParam String owner, @RequestParam String serial) {
        try {
            log.info("Starting product validation process...");
            return new ResponseEntity<>(validationService.validate(owner, serial), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Fatal Error: {}", e.getMessage());
            log.error("Caused by", e.getCause());
            return new ResponseEntity<>(new ProductValidationResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
