package edu.ues.safetylproductauthenticatorservice.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class MailModel {

    private String from;
    private String to;
    private String subject;
    private String template;
    private Map<String, Object> model = new HashMap<>();

}
