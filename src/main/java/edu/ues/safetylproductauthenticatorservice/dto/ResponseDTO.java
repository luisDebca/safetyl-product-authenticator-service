package edu.ues.safetylproductauthenticatorservice.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ResponseDTO {

    private String message;
    private Object data;
    private LocalDateTime dateTime;

    public ResponseDTO ok(Object o) {
        message = "OK";
        dateTime = LocalDateTime.now();
        data = o;
        return this;
    }

    public ResponseDTO error() {
        message = "ERROR";
        dateTime = LocalDateTime.now();
        return this;
    }
}
