package edu.ues.safetylproductauthenticatorservice.dto;

import lombok.Data;

@Data
public class ProductValidationRequest {

    private String ownerName;
    private String serial;
}
