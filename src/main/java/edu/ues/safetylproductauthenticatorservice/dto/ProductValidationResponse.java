package edu.ues.safetylproductauthenticatorservice.dto;

import edu.ues.safetylproductauthenticatorservice.model.VerificationResult;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ProductValidationResponse {

    private boolean validationResult;
    private LocalDateTime validateAt;
    private LocalDateTime expiredAt;
    private List<String> products;
    private String nextValidationCode;
    private VerificationResult verificationResult;
}
