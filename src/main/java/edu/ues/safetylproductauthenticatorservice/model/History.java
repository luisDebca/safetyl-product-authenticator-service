package edu.ues.safetylproductauthenticatorservice.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private License license;

    @Column
    private LocalDateTime verificationDate;

    @Column(length = 100)
    private String productOwner;

    @Column(length = 100)
    private String productSerial;

    @Column(length = 500)
    private String productList;

    @Column
    @Enumerated(value = EnumType.STRING)
    private VerificationResult verificationResult;

}
