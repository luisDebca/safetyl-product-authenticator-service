package edu.ues.safetylproductauthenticatorservice.model;

public enum VerificationResult {
    SUCCESS, FAIL, EXPIRED
}
