package edu.ues.safetylproductauthenticatorservice.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class UserApp {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(length = 15, nullable = false, unique = true)
    private String username;

    @Column(length = 100, nullable = false)
    private String password;

    @Column(length = 150, nullable = false, unique = true)
    private String email;
}
