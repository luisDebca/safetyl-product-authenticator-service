package edu.ues.safetylproductauthenticatorservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class License {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Owner owner;

    @Column(length = 100, nullable = false)
    private String serial;

    @Column
    private LocalDateTime activationDate;

    @Column
    private LocalDateTime expireDate;

    @Column
    private LocalDateTime lastVerification;

    @Column
    private Integer maxUserEnabled;

    @Column
    private Integer maxInstanceEnable;

    @Column
    private Boolean enable;

    @OneToMany(mappedBy = "license", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<History> histories = new ArrayList<>();

    @OneToMany(mappedBy = "license", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Instance> instances = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "product_license", joinColumns = @JoinColumn(name = "license_id"), inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products = new ArrayList<>();

    private void addProduct(Product product) {
        products.add(product);
        product.getLicenses().add(this);
    }

    private void removeProduct(Product product) {
        products.remove(product);
        product.getLicenses().remove(this);
    }

}
