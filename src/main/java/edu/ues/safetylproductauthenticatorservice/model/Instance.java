package edu.ues.safetylproductauthenticatorservice.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Instance {

    @Id
    private String uuid;

    @Column(nullable = false, length = 45)
    private String ip;

    @Column(nullable = false, length = 23)
    private String mac;

    @ManyToOne(fetch = FetchType.LAZY)
    private License license;

}
