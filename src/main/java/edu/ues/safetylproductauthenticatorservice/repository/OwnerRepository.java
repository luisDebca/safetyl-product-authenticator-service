package edu.ues.safetylproductauthenticatorservice.repository;

import edu.ues.safetylproductauthenticatorservice.model.Owner;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "owners", path = "owners")
public interface OwnerRepository extends PagingAndSortingRepository<Owner, Long> {

    Owner findByName(String name);
}
