package edu.ues.safetylproductauthenticatorservice.repository;

import edu.ues.safetylproductauthenticatorservice.model.History;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "histories", path = "histories")
public interface HistoryRepository extends PagingAndSortingRepository<History, Long> {
}
