package edu.ues.safetylproductauthenticatorservice.repository;

import edu.ues.safetylproductauthenticatorservice.model.UserApp;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends PagingAndSortingRepository<UserApp, Long> {

    UserApp findByUsername(String username);

}
