package edu.ues.safetylproductauthenticatorservice.repository;

import edu.ues.safetylproductauthenticatorservice.model.License;
import edu.ues.safetylproductauthenticatorservice.model.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.time.LocalDateTime;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "licenses", path = "licenses")
public interface LicenseRepository extends PagingAndSortingRepository<License, Long> {

    @Modifying
    @Query("update License l set l.enable = :enable where l.id = :id")
    void enable(@Param("id") Long id, @Param("enable") boolean enable);

    @Query("select l from Owner o join o.licenses l where o.name = :name")
    List<License> getByOwner(@Param("name") String name);

    @Modifying
    @Query("update License l set l.lastVerification = :last where l.id = :id")
    void updateLastVerification(@Param("id") Long id, @Param("last")LocalDateTime dateTime);

    @Query("select p from License l join l.products p where l.id = :id")
    List<Product> findAllProducts(@Param("id") Long id);
}
